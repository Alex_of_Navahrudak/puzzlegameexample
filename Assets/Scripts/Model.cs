//The delegates play a role of Game Manager, connecting the Model and the Game
public delegate void SetFigure(int x, int y, FigureType type, bool justOutline);
public delegate void DeclareVictory();

//The Model is responcible for the accounting and registration and control of the cells' essence:
//their flags (filled/unfilled figure), types of the placed figure, etc.
public class Model
{
    public const int SIZE = 3;
    public const int FIGURESCOUNT = 5;

    private SetFigure setFigure;
    private DeclareVictory declareVictory;

    private FigureType[,] grid; //Cells types
    private bool[,] marks = new bool[SIZE, SIZE]; //Cells flags (filled/unfilled)

    private System.Random random = new System.Random();

    private int filledFigures = 0;
    private int FilledFigures
    {
        get => filledFigures;
        set
        {
            if (value < SIZE * SIZE && value > 0)
            {
                filledFigures = value;
            }
            else if (value > 0)
            {
                filledFigures = 0;
                declareVictory.Invoke(); //When the placed figures count equals the cells count, we'll open the win panel
            }
        }
    }

    public Model (SetFigure setFigure, DeclareVictory declareVictory)
    {
        this.setFigure = setFigure;
        this.declareVictory = declareVictory;
        grid = new FigureType[SIZE, SIZE];
    }

    //Fills the empty cell fith the concidental figure
    public void PutFigure(int x, int y, FigureType figureType)
    {
        marks[x, y] = true;
        setFigure(x, y, figureType, false);
        FilledFigures++;
    }

    //Compares the cursor figure type with the touched cell type
    public void CheckTriggeredFigure(int x, int y, FigureType figureType)
    {
        if (!marks[x, y] && grid[x, y] == figureType)
        {
            PutFigure(x, y, figureType);
        }
    }

    //Fills a cell by a random empty figure (outlines)
    public void SetRandomOutline(int x, int y)
    {
        int randomOutline = random.Next(FIGURESCOUNT);
        grid[x, y] = (FigureType)randomOutline;
        setFigure(x, y, (FigureType)randomOutline, true);
    }
        
    public void SetRandomOutlines()
    {
        for (int i = 0; i < SIZE; i++)
            for (int j = 0; j < SIZE; j++)
                SetRandomOutline(i, j);
    }

    //Refreshes the current game field (it doesn't reset the game, just decolor the current figures on the field)
    public void ReplayGameField()
    {
        for (int i = 0; i < SIZE; i++)
            for (int j = 0; j < SIZE; j++)
            {
                marks[i, j] = false;
                setFigure(i, j, grid[i, j], true);
            }
    }

}

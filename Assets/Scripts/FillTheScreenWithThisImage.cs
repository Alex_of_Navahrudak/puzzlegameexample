using UnityEngine;
using UnityEngine.UI;

//This script allows us to be sure that a scroll rect form (in main menu) have the same
//size as the screen, in spite of the smartphone sides ratio
public class FillTheScreenWithThisImage : MonoBehaviour
{
    [SerializeField] private Canvas canvas;

    void Start()
    {
        Image thisImage = GetComponent<Image>();
        Vector2 canvasSize = canvas.GetComponent<RectTransform>().rect.size;

        thisImage.rectTransform.sizeDelta = canvasSize;
    }
}

using UnityEngine;
using UnityEngine.UI;

public enum FigureType
{
    Circle,
    Triangle,
    Rectangle,
    Mosaic,
    Star
}

//This class controls the graphical representation of the game
public class Game : MonoBehaviour
{
    [SerializeField] private CursorFigure cursorFigure;
    [SerializeField] private GameObject winCanvasObj;

    [SerializeField] private Sprite[] figures;
    [SerializeField] private Sprite[] outlines;

    private FigureType activatedFigure;
    private Model model;

    private Image[,] cells;

    //Generates a brand new set of empty figures (outlines) on the game field
    public void ResetGame()
    {
        InitCells();
        model = new Model(SetFigure, DeclareVictory);
        model.SetRandomOutlines();
    }

    //Flls a cell on the game field with an empty or filled figure
    public void SetFigure(int x, int y, FigureType type, bool justOutline)
    {
        cells[x, y].GetComponent<Image>().sprite = justOutline ?
            outlines[(int)type]
            : figures[(int)type];

        //If the type of the cursor figure equals the type of the touched cell,
        //the outline is filled and the cursor figure should be deactivated
        if (!justOutline)
            cursorFigure.Deactivate(); 
    }

    //Sets the type of the cursor figure according to the selected one
    public void TakeBarFigure(int index)
    {
        FigureType figureType = (FigureType)index;

        cursorFigure.gameObject.SetActive(true);
        cursorFigure.Activate(figures[(int)figureType]);
        activatedFigure = figureType;
    }

    //When the cursor figure touches another one, the CursorModel component signalize about this fact and the Model compare their types
    public void CheckTriggeredFigure(int x, int y)
    {
        model.CheckTriggeredFigure(x, y, activatedFigure);
    }

    //Fills the cells with the links on the Image components
    private void InitCells()
    {
        cells = new Image[Model.SIZE, Model.SIZE];

        for (int i = 0; i < cells.Length; i++)
        {
            GameObject cellImgObj = GameObject.Find($"FigureField ({i})");
            Image cellImg = cellImgObj.GetComponent<Image>();
            cells[i % Model.SIZE, i / Model.SIZE] = cellImg;
        }
    }

    public void DeclareVictory()
    {
        winCanvasObj.SetActive(true);
        gameObject.SetActive(false);
    }

    public void ReplayGame()
    {
        model.ReplayGameField();
    }
}

using System;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

//This script controls the figure image which follows the ursr after dragging any figure from the bottom bar
public class CursorFigure : MonoBehaviour
{
    [SerializeField] private Game gameScript;
    [SerializeField] private Canvas parentCanvas;

    private Image img;

    private void Awake()
    {
        img = GetComponent<Image>();
    }

    public void Update()
    {
        FollowCursor();

        if (Input.GetMouseButtonUp(0))
        {
            Deactivate();
        }
    }

    //The figure follows the cursor focusing on the canvas borders
    private void FollowCursor()
    {
        Vector2 movePos;

        RectTransformUtility.ScreenPointToLocalPointInRectangle(
            parentCanvas.transform as RectTransform,
            Input.mousePosition, parentCanvas.worldCamera,
            out movePos);

        transform.position = parentCanvas.transform.TransformPoint(movePos);
    }

    //If the figure touches another one, the Model compare their types
    private void OnTriggerEnter2D(Collider2D collision)
    {
        int cellNumber = GetNumber(collision.name);
        int x = cellNumber % Model.SIZE;
        int y = cellNumber / Model.SIZE;
        
        gameScript.CheckTriggeredFigure(x, y);
    }

    //Getting the cells number by their names
    private int GetNumber(string name)
    {
        Regex regex = new Regex("\\((\\d+)\\)");
        Match match = regex.Match(name);
        if (!match.Success)
            throw new System.Exception("Please check the cell name...");
        Group group = match.Groups[1];
        string number = group.Value;
        return Convert.ToInt32(number);
    }

    //Before deactivation, the figure should be discolored to avoid twitching when it'll appear again
    public void Deactivate()
    {
        img.color = Color.clear;
        gameObject.SetActive(false);
    }

    //When the figure reaches the cursor position, we color it
    public void Activate(Sprite figureSprite)
    {
        FollowCursor();
        img.sprite = figureSprite;
        img.color = Color.white;
    }
}
